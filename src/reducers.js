/* 
 * This is what I want the inital state of the store to be on load.
 * I'm utilising es6 default values in the constructor of app to set
 * state to initialState if state is undefined - which it will be only 
 * at runtime.
 */
const initialState = {
    posts: {
        page: 0,
        items: []
    }
}

// This is the reducer object
const app = (state = initialState, action) => {
    switch(action.type) {
        case 'LOAD_POSTS':

            // got to make sure that both state & action are not mutated so I'm creating a new immutable posts object
            const posts = {
                items: state.posts.items.concat(action.posts.items), // concat returns a new array not mutating existing one
                page: action.posts.page
            }

            return Object.assign({}, state, {posts}) // again, Object.assign writes from left to right. I'm starting with a fresh object to not mutate existing state
        default:
            return state
    }
}

export default app;
