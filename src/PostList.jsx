import React                    from 'react';
import {connect}                from 'react-redux';
import {bindActionCreators}     from 'redux';
import {loadPosts}              from './actions.js';

// static react component, destructuring props in the constructor
const PostList = ({page, items, loadPosts}) => (
    <div>
        <h1>Page {page}</h1>

        {items.length ?
            <ul>
                {items.map((item, i) => <li key={i}>{item}</li>)}
            </ul>
        : null}

        <button onClick={() => loadPosts(page+1)}>Load More</button>
    </div>
)

/**
 * We pass this function into connect() from react-redux to tell it
 * how/what to map from the store into our component above.
 *
 * In this instance I'm telling it to only give us the posts object from store
 */
function mapStateToProps(state) {
    return state.posts;
}

/**
 * This is the shorthand notation for mapping actions to component props in the same
 * way that the function above is mapping the store state to props.
 *
 * In this instance I'm telling it to give us the loadPosts action
 */
const mapDispatchToProps = {
  loadPosts
}

/*
 * Here we export our PostList component whose props will be provided by the connect function 
 * from react-redux which is in turn getting them from redux (for store state) and 
 * what I've imported (for actions)
 */
export default connect(mapStateToProps, mapDispatchToProps)(PostList)
