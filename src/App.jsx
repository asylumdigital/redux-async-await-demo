import React, {Component}       from 'react';
import {Provider}               from 'react-redux';
import {
    createStore, 
    applyMiddleware
} from 'redux';
import ReduxThunk               from 'redux-thunk'
import app                      from './reducers.js';
import PostList                 from './PostList.jsx';

// using redux to create the store with reducers
let store = createStore(
    app,
    applyMiddleware(ReduxThunk) // middleware allows us to return functions from actions
);

class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Provider store={store}>
                <PostList />
            </Provider>
        )

    }
}

export default App;
